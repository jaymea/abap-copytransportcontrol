**********************************************************************
*
*  Programa para criação de transporte de cópias de requests
*  Versão 0.81 - 04.06.2020
*  Autor: Jayme Alonso
*
**********************************************************************
*  Funcionalidades
*
*  1. Exibe informações das requests de workbench e cópia.
*  2. Permite a navegação para o LOG de transporte das requests e
*     lista de objetos.
*  3. Cria uma cópia da request de workbench contendo todos os
*     objetos da request original e das tasks abaixo dela.
*  4. Libera a request de transporte de cópias para o QAS e realiza
*     a importação sem precisar acessar a transação STMS.
*
*  Obs: - Este programa não tem intuito de substituir o Transport
*       Organizer. (SE01/SE09/SE10)
*       - Este programa foi desenhado para ser transportável para
*       outros ambientes com um esforço mínimo. Por este motivo isso
*       não foram criados STATUS GUIs / TITLEs e outros objetos que
*       são mais difíceis de documentar.
*
*  BUG - O erro abaixo é conhecido:
*    Após a liberação da request existe uma demora de 5-10 segundos
*    antes que se possa realizar a importação da request no QAS (STMS).
*    Caso não exista a espera esse tempo é possivel que apareça um pop
*    up com um erro no transporte. Enquanto esse bug não é tratado,
*    como alternativa, é possível repetir o transporte até que ele dê
*    certo.
*
*  ANTES DE EXECUTAR:
*  1: Modificar as CONSTANTS abaixo de acordo com o ambiente destino:
*      lcl_transporte_copia=>c_qa_sysid
*      lcl_transporte_copia=>c_qa_client
*
*  2: Adicionar Textos:
*      P_OWNER Usuário "dono" da request
*      P_UPTO  Max reg.
*      S_REQ   Ordem/tarefa
*
**********************************************************************
* 03.02.2020 - Versão 0.5
*              - Inicial
* 26.02.2020 - Versão 0.6
*              - Adicionado Request Superior Baseado em relação da
*              tabela E071
* 27.02.2020 - Versão 0.7
*              - Adicionando exibição de detalhes da request quando
*              clica na coluna
*              - Atualizando contagem de requests quando cria uma nova
* 08.05.2020 - Versão 0.8
*              - Removendo a pergunta se deseja copiar a request, pois
*              não é necessário devido ao fato que a criação pode ser
*              cancelada no pop up exibido.
* 04.06.2020 - Versão 0.81
*              - Renomeando zteste_jayme_tr_copias >> ztr_copias
**********************************************************************
REPORT ztr_copias.

CLASS lcl_popup DEFINITION.
  PUBLIC SECTION.
    CLASS-METHODS confirm_yes_no
      IMPORTING question             TYPE string
      RETURNING VALUE(confirmed_yes) TYPE sap_bool.
ENDCLASS.
CLASS lcl_popup IMPLEMENTATION.

  METHOD confirm_yes_no.

    DATA answer TYPE char1.
    CALL FUNCTION 'POPUP_TO_CONFIRM'
      EXPORTING
*       titlebar              = SPACE
*       diagnose_object       = SPACE
        text_question         = question
*       text_button_1         = TEXT-001
*       icon_button_1         = SPACE
*       text_button_2         = TEXT-002
*       icon_button_2         = SPACE
*       default_button        = '1'
        display_cancel_button = ''
*       userdefined_f1_help   = SPACE
*       start_column          =
*       start_row             =
*       popup_type            =
*       iv_quickinfo_button_1 = SPACE
*       iv_quickinfo_button_2 = SPACE
      IMPORTING
        answer                = answer
*    TABLES
*       parameter             =
      EXCEPTIONS
        text_not_found        = 1
        OTHERS                = 2.
    IF sy-subrc <> 0.
*   MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    confirmed_yes = xsdbool( answer = '1' ).

  ENDMETHOD.

ENDCLASS.


CLASS lcl_transporte_copia DEFINITION.
  PUBLIC SECTION.

    CONSTANTS:
      c_qa_sysid  TYPE sy-sysid VALUE 'R3Q',
      c_qa_client TYPE mandt VALUE '003'.

    TYPES:
      BEGIN OF ty_request,
        icon            TYPE string, " char5,
        icon_txt        TYPE string,
        icon_tr_log     TYPE string, " char5,
        icon_tr_log_txt TYPE string.
            INCLUDE TYPE e070.
    TYPES:
      langu   TYPE e07t-langu,
      as4text TYPE e07t-as4text.
    TYPES:
      END OF ty_request.
    TYPES: tyt_requests TYPE TABLE OF ty_request WITH EMPTY KEY.
    TYPES tyr_trkorr TYPE RANGE OF e070-trkorr.
    TYPES: BEGIN OF ty_selection_input,
             requests_range TYPE tyr_trkorr,
             owner          TYPE sy-uname,
             upto           TYPE i,
           END OF ty_selection_input.

    METHODS select_data.
    METHODS set_selection_input IMPORTING input TYPE ty_selection_input.
    METHODS get_requests RETURNING VALUE(requests) TYPE tyt_requests.

    METHODS cria_copia.
    METHODS libera_ordem.
    METHODS importa_destination.

    DATA requests TYPE tyt_requests.
    DATA request_selected TYPE ty_request.

  PRIVATE SECTION.
    DATA selection TYPE ty_selection_input.

ENDCLASS.

CLASS lcl_transporte_copia IMPLEMENTATION.
  METHOD select_data.


    IF me->selection-requests_range IS INITIAL.
      DATA lr_involved TYPE RANGE OF e070-trkorr.
      SELECT 'I' AS sign, 'EQ' AS option, strkorr AS low
        FROM e070 AS e7
       WHERE as4user = @me->selection-owner AND strkorr <> ''
             INTO TABLE @lr_involved.
    ENDIF.

    CLEAR me->requests.
    CLEAR me->request_selected.

    SELECT @space AS icon,
           @space AS icon_txt,
           @space AS icon_tr_log,
           @space AS icon_tr_log_txt,
           e7~*, e7t~langu, e7t~as4text
      FROM e070 AS e7
           INNER JOIN e07t AS e7t ON e7t~trkorr = e7~trkorr
      WHERE e7~as4user = @me->selection-owner
        AND e7~trkorr IN @me->selection-requests_range
        AND (
              ( e7~trfunction = 'K'    " workbench
            AND e7~trstatus   = 'D' )  " modificavel
             OR
              ( e7~trfunction = 'T' )  " copia
            )
            INTO TABLE @me->requests UP TO @me->selection-upto ROWS.

    IF NOT lr_involved IS INITIAL.
      SELECT @space AS icon,
             @space AS icon_txt,
             @space AS icon_tr_log,
             @space AS icon_tr_log_txt,
             e7~*, e7t~langu, e7t~as4text
        FROM e070 AS e7
             INNER JOIN e07t AS e7t ON e7t~trkorr = e7~trkorr
       WHERE e7~trkorr IN @lr_involved AND e7~as4user <> @me->selection-owner
              APPENDING TABLE @me->requests.
    ENDIF.

    DATA lt_log_overview TYPE scts_log_overviews .
    DATA lv_check TYPE i.
    LOOP AT me->requests ASSIGNING FIELD-SYMBOL(<request>).

      IF <request>-trstatus = 'R'. " Liberado
        CLEAR lt_log_overview.
        CALL FUNCTION 'TRINT_GET_LOG_OVERVIEW'
          EXPORTING
            iv_request                = <request>-trkorr
            iv_with_transport_targets = 'X'
          IMPORTING
            et_log_overview           = lt_log_overview.
        lv_check = 0.
        <request>-icon_tr_log = COND #( WHEN NOT lt_log_overview IS INITIAL THEN icon_display_text ELSE '' ).
        <request>-icon_tr_log_txt = COND #( WHEN NOT lt_log_overview IS INITIAL THEN 'Verifica log de transporte' ELSE '' ).

        DATA lv_erro TYPE sap_bool.
        CLEAR lv_erro.


        READ TABLE lt_log_overview INTO DATA(ls_log_overview) WITH KEY sysnam = c_qa_sysid.
        IF sy-subrc = 0.

          CONDENSE ls_log_overview-rc NO-GAPS.
          CASE ls_log_overview-rc.
            WHEN '8' OR  '12' OR '16' OR  'E'. " São erros ? Erro ?
              <request>-icon = icon_error_protocol.
              <request>-icon_txt = 'Erro'.
              CONTINUE.
            WHEN 'D'. " eliminado da fila
              <request>-icon = icon_breakpoint.
              <request>-icon_txt = 'Eliminado da fila'.
              CONTINUE.
            WHEN 'W'. " waiting ?
              <request>-icon = icon_led_inactive.
              <request>-icon_txt = 'Aguardando importação...'.
              CONTINUE.
            WHEN OTHERS.
          ENDCASE.
        ENDIF.
*
*      LOOP AT lt_log_overview INTO DATA(ls_log_overview) WHERE sysnam = c_qa_sysid.
*        CONDENSE ls_log_overview-rc NO-GAPS.
*        IF ls_log_overview-rc = '8' or ls_log_overview-rc = '12' OR ls_log_overview-rc = '16' OR ls_log_overview-rc = 'E'.
*          lv_erro = abap_true.
*          EXIT.
*        ENDIF.
*      ENDLOOP.
*      IF lv_erro = abap_true.
*        <request>-icon = icon_error_protocol.
*        CONTINUE.
*      ENDIF.
      ENDIF.

      CASE <request>-trfunction.
        WHEN 'K'. " workbench.
          <request>-icon = COND #( WHEN <request>-trstatus = 'D' THEN icon_copy_object ELSE icon_okay ).
          <request>-strkorr = <request>-trkorr.
          <request>-icon_txt = COND #( WHEN <request>-trstatus = 'D' THEN 'Criar Request de Transporte de Cópias' ELSE 'Okay' ).
        WHEN 'T'. " copias
          <request>-icon = COND #( WHEN <request>-trstatus = 'D' THEN icon_transport ELSE icon_okay ).
          <request>-icon_txt = COND #( WHEN <request>-trstatus = 'D' THEN |Liberar request e importar no "{ c_qa_sysid }"...| ELSE 'Okay' ).
      ENDCASE.
    ENDLOOP.

    " Add Tooltip
    LOOP AT me->requests ASSIGNING <request>.
      IF NOT <request>-icon IS INITIAL.
        <request>-icon        = <request>-icon(3) && '\Q' && <request>-icon_txt && <request>-icon+3(1).
      ENDIF.
      IF NOT <request>-icon_tr_log IS INITIAL.
        <request>-icon_tr_log = <request>-icon_tr_log(3) && '\Q' && <request>-icon_tr_log_txt && <request>-icon_tr_log+3(1).
      ENDIF.
    ENDLOOP.


    " Buscar request superior
    IF NOT me->requests IS INITIAL.
      SELECT trkorr, obj_name
        FROM e071 FOR ALL ENTRIES IN @me->requests
       WHERE trkorr = @me->requests-trkorr
         AND pgmid  = 'CORR'
         AND object = 'MERG'
             INTO TABLE @DATA(lt_071).

      TYPES: BEGIN OF ty_requests_relation,
               req_copia TYPE e070-trkorr,
               req_super TYPE e070-trkorr,
             END   OF ty_requests_relation.

      DATA lt_requests_relation TYPE TABLE OF ty_requests_relation.
      lt_requests_relation = VALUE #(
        FOR i IN lt_071 (
          req_copia = i-trkorr
          req_super = i-obj_name(10)
        )
      ).
      SORT lt_requests_relation.
      DELETE ADJACENT DUPLICATES FROM lt_requests_relation.

      " Valida quais das requests superiores que foram "MERGED" são workbench,
      " somente estas devem ser válidas como superiores
      IF NOT lt_requests_relation IS INITIAL.
        SELECT * FROM e070 FOR ALL ENTRIES IN @lt_requests_relation
         WHERE " trfunction = 'K' " WORKBENCH
           " AND
               trkorr = @lt_requests_relation-req_super
               INTO TABLE @DATA(lt_main_req).

        LOOP AT lt_requests_relation ASSIGNING FIELD-SYMBOL(<ls_requests_relation>).
          DATA(lv_tabix) = sy-tabix.
          READ TABLE lt_main_req INTO DATA(ls_main_req) WITH KEY trkorr = <ls_requests_relation>-req_super.

          IF sy-subrc <> 0.
            DELETE lt_requests_relation INDEX lv_tabix.
          ELSE.
            CASE ls_main_req-trfunction.
              WHEN 'K'. " workbench
                <ls_requests_relation>-req_super = ls_main_req-trkorr.
              WHEN 'S' or 'R' or 'T'.
                <ls_requests_relation>-req_super = ls_main_req-strkorr.
            ENDCASE.
          ENDIF.
        ENDLOOP.
      ENDIF.

      LOOP AT me->requests ASSIGNING <request>.
        READ TABLE lt_requests_relation ASSIGNING <ls_requests_relation>
          WITH KEY req_copia = <request>-trkorr.
        IF sy-subrc = 0.
          <request>-strkorr = <ls_requests_relation>-req_super.
        ENDIF.
      ENDLOOP.

    ENDIF.


    SORT me->requests BY strkorr as4date as4time.


  ENDMETHOD.

  METHOD get_requests.
    requests = me->requests.
  ENDMETHOD.

  METHOD set_selection_input.
    me->selection = input.
  ENDMETHOD.

  METHOD libera_ordem.

    DATA:
      ls_request       TYPE trwbo_request,
      lt_deleted_tasks TYPE trwbo_t_e070,
      lt_messages      TYPE ctsgerrmsgs.


    CALL FUNCTION 'TRINT_RELEASE_REQUEST'
      EXPORTING
        iv_trkorr                   = me->request_selected-trkorr
        iv_dialog                   = ' '
        iv_without_locking          = 'X'
      IMPORTING
        es_request                  = ls_request
        et_deleted_tasks            = lt_deleted_tasks
        et_messages                 = lt_messages
      EXCEPTIONS
        cts_initialization_failure  = 1
        enqueue_failed              = 2
        no_authorization            = 3
        invalid_request             = 4
        request_already_released    = 5
        repeat_too_early            = 6
        object_lock_error           = 7
        object_check_error          = 8
        docu_missing                = 9
        db_access_error             = 10
        action_aborted_by_user      = 11
        export_failed               = 12
        execute_objects_check       = 13
        release_in_bg_mode          = 14
        release_in_bg_mode_w_objchk = 15
        error_in_export_methods     = 16
        object_lang_error           = 17
        OTHERS                      = 18.


    CALL FUNCTION 'PROGRESS_INDICATOR'
      EXPORTING
        i_text               = |Ordem { me->request_selected-trkorr } liberada.|
        i_output_immediately = abap_true.

    DATA lt_log_overview TYPE scts_log_overviews.
    DATA lv_check TYPE i.
    DO 4 TIMES.
      CALL FUNCTION 'TRINT_GET_LOG_OVERVIEW'
        EXPORTING
          iv_request                = me->request_selected-trkorr
          iv_with_transport_targets = 'X'
        IMPORTING
          et_log_overview           = lt_log_overview.
      lv_check = 0.
      LOOP AT lt_log_overview TRANSPORTING NO FIELDS WHERE sysnam = lcl_transporte_copia=>c_qa_sysid AND rc = 'W'.
      ENDLOOP.
      IF sy-subrc = 0.
        lv_check = 1.
      ENDIF.
      IF lv_check = 1.
        EXIT.
      ENDIF.
      WAIT UP TO 1 SECONDS.
    ENDDO.

    IF lv_check <> 1.
      MESSAGE i398(00) WITH |Erro na liberação da ordem { me->request_selected-trkorr }.|.
    ELSE.
      MESSAGE i398(00) WITH |Ordem { me->request_selected-trkorr } LIBERADA com sucesso !|.
*      me->importa_destination( iv_ordem = iv_ordem ).
    ENDIF.

  ENDMETHOD.

  METHOD importa_destination.

    DATA lv_trretcode TYPE trretcode .
    DATA ls_exception TYPE stmscalert.
    CALL FUNCTION 'TMS_MGR_IMPORT_TR_REQUEST'
      EXPORTING
        iv_system                  = CONV tmssysnam( lcl_transporte_copia=>c_qa_sysid )
        iv_request                 = me->request_selected-trkorr
        iv_client                  = lcl_transporte_copia=>c_qa_client
        iv_ignore_cvers            = 'X'
      " iv_monitor                 = 'X'
        iv_verbose                 = 'X'
      IMPORTING
        ev_tp_ret_code             = lv_trretcode
        es_exception               = ls_exception
      EXCEPTIONS
        read_config_failed         = 1
        table_of_requests_is_empty = 2
        OTHERS                     = 3.

    IF sy-subrc <> 0.
      " DEU ERRO!
      BREAK-POINT.
    ELSE.
      MESSAGE i398(00) WITH |Ordem { me->request_selected-trkorr } importada com sucesso no ambiente { lcl_transporte_copia=>c_qa_sysid }.|.
    ENDIF.

  ENDMETHOD.

  METHOD cria_copia.

    DATA lv_text TYPE as4text.
    DATA lv_request_header TYPE trwbo_request_header.
    DATA lv_task_headers TYPE trwbo_request_headers .

    CONSTANTS c_end_append_text TYPE string VALUE '(cp01)'.
    DATA(lv_end_text_size) = strlen( c_end_append_text ).

    lv_text = request_selected-as4text.
    DATA(end_length) = strlen( lv_text ).
    IF end_length > ( 60 - lv_end_text_size ). " se for maior que o tamanho total do texto menos o que tem que ser adicionado.
      end_length = 60 - lv_end_text_size. " (Cop01) <<< 7 chars
    ENDIF.
    lv_text+end_length = c_end_append_text.


    DATA lv_retcode TYPE c.
    TYPES tyt_sval TYPE TABLE OF sval WITH EMPTY KEY.
    DATA(lt_fields) = VALUE tyt_sval(
      ( tabname = 'E07T' fieldname = 'AS4TEXT' value = lv_text )
    ).
    CALL FUNCTION 'POPUP_GET_VALUES'
      EXPORTING
*       no_value_check  = SPACE
        popup_title     = 'nome da request'
*       start_column    = '5'
*       start_row       = '5'
      IMPORTING
        returncode      = lv_retcode
      TABLES
        fields          = lt_fields
      EXCEPTIONS
        error_in_fields = 1
        OTHERS          = 2.
    IF sy-subrc <> 0 OR NOT lv_retcode IS INITIAL.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      RETURN.
    ENDIF.

    lv_text = lt_fields[ tabname = 'E07T' fieldname = 'AS4TEXT' ]-value.

    CALL FUNCTION 'TR_INSERT_REQUEST_WITH_TASKS'
      EXPORTING
        iv_type           = 'T'
        iv_text           = lv_text
        iv_owner          = sy-uname
        iv_target         = CONV tr_target( lcl_transporte_copia=>c_qa_sysid )
      IMPORTING
        es_request_header = lv_request_header
        et_task_headers   = lv_task_headers
      EXCEPTIONS
        insert_failed     = 1
        enqueue_failed    = 2
        OTHERS            = 3.


    SELECT trkorr FROM e070
     WHERE strkorr = @request_selected-trkorr
      INTO TABLE @DATA(lt_tasks).


    CALL FUNCTION 'TR_COPY_COMM'
      EXPORTING
        wi_dialog                = abap_false
        wi_trkorr_from           = request_selected-trkorr
        wi_trkorr_to             = lv_request_header-trkorr
        wi_without_documentation = abap_false
      EXCEPTIONS
        db_access_error          = 1
        trkorr_from_not_exist    = 2
        trkorr_to_is_repair      = 3
        trkorr_to_locked         = 4
        trkorr_to_not_exist      = 5
        trkorr_to_released       = 6
        user_not_owner           = 7
        no_authorization         = 8
        wrong_client             = 9
        wrong_category           = 10
        object_not_patchable     = 11
        OTHERS                   = 12.

    LOOP AT lt_tasks INTO DATA(ls_task).

      CALL FUNCTION 'TR_COPY_COMM'
        EXPORTING
          wi_dialog                = abap_false
          wi_trkorr_from           = ls_task-trkorr
          wi_trkorr_to             = lv_request_header-trkorr
          wi_without_documentation = abap_false
        EXCEPTIONS
          db_access_error          = 1
          trkorr_from_not_exist    = 2
          trkorr_to_is_repair      = 3
          trkorr_to_locked         = 4
          trkorr_to_not_exist      = 5
          trkorr_to_released       = 6
          user_not_owner           = 7
          no_authorization         = 8
          wrong_client             = 9
          wrong_category           = 10
          object_not_patchable     = 11
          OTHERS                   = 12.

    ENDLOOP.

  ENDMETHOD.

ENDCLASS.

CLASS lcl_alvgrid DEFINITION.

  PUBLIC SECTION.
    METHODS chama_alv.
    METHODS set_transporte_copia IMPORTING object TYPE REF TO lcl_transporte_copia.
    METHODS update_line_count.
  PRIVATE SECTION.

    METHODS:
      on_line_click FOR EVENT link_click OF cl_salv_events_table
        IMPORTING row column.

    DATA:
      r_data TYPE REF TO lcl_transporte_copia,
      o_alv  TYPE REF TO cl_salv_table.


ENDCLASS.

CLASS lcl_alvgrid IMPLEMENTATION .

  METHOD chama_alv.

    DATA: lo_functions TYPE REF TO cl_salv_functions_list,
          lo_displayst TYPE REF TO cl_salv_display_settings.

    DATA: lx_msg TYPE REF TO cx_salv_msg.
    DATA: lt_msg TYPE TABLE OF bal_s_msg,
          ls_msg TYPE bal_s_msg.

    DATA: lv_linhas TYPE char5,
          lv_title  TYPE lvc_title.

    TRY.

        " data(lt_Req) = r_data->get_requests( ).
        DATA(lo_data) = me->r_data.
        cl_salv_table=>factory(
          IMPORTING
            r_salv_table = o_alv
          CHANGING
            t_table      = lo_data->requests ).


*       Set Standard Toolbar ALL Functions
*  *********************************************************************
        lo_functions = o_alv->get_functions( ).
        lo_functions->set_all( abap_true ).

*       Set Save Layout
*  *********************************************************************
        DATA: lo_layout TYPE REF TO cl_salv_layout.
        DATA: ls_key TYPE salv_s_layout_key.

        ls_key-report = sy-repid.

        lo_layout = o_alv->get_layout( ).
        lo_layout->set_key( ls_key ).
        lo_layout->set_save_restriction( if_salv_c_layout=>restrict_none ).


*       Display Settings
*  *********************************************************************
        lo_displayst = o_alv->get_display_settings( ).
        lo_displayst->set_striped_pattern( abap_true ).

        update_line_count(  ).
*     Columns
**********************************************************************
        DATA lo_cols_t TYPE REF TO cl_salv_columns_table.
        DATA lo_col_t  TYPE REF TO cl_salv_column_table.
        DATA: lt_cols TYPE salv_t_column_ref,
              ls_col  TYPE salv_s_column_ref,
              lo_col  TYPE REF TO cl_salv_column.
        TRY .

            lo_cols_t = o_alv->get_columns( ).
            lt_cols = lo_cols_t->get( ).

            LOOP AT lt_cols INTO ls_col.
              lo_col = ls_col-r_column.
              lo_col_t ?= ls_col-r_column.
              CASE ls_col-columnname.
                WHEN 'ICON' OR 'ICON_TR_LOG'.
                  lo_col->set_alignment( if_salv_c_alignment=>centered ).
                  lo_col->set_output_length( 5 ).
                  lo_col_t->set_cell_type( value = if_salv_c_cell_type=>hotspot ).

*                  lo_col_t->get_ set_t( value = ls_col-columnname && '_TXT' ).
*                    CATCH cx_salv_not_found.    "
*                    CATCH cx_salv_data_error.    "
                WHEN 'TRKORR'.
                  lo_col_t->set_cell_type( value = if_salv_c_cell_type=>hotspot ).
                WHEN 'ICON_TXT' OR 'ICON_TR_LOG_TXT'.
                  lo_col->set_technical( ).
                WHEN OTHERS.
              ENDCASE.

            ENDLOOP.

          CATCH cx_salv_not_found.

        ENDTRY.

**********************************************************************
        DATA(lo_events) = o_alv->get_event( ).
        SET HANDLER me->on_line_click FOR lo_events.

*       Display ALV
        o_alv->display( ).

      CATCH cx_salv_msg INTO lx_msg.
        ls_msg = lx_msg->get_message( ).

        MESSAGE ID ls_msg-msgid TYPE ls_msg-msgty NUMBER ls_msg-msgno
           WITH ls_msg-msgv1 ls_msg-msgv2 ls_msg-msgv3 ls_msg-msgv4.
    ENDTRY.

  ENDMETHOD.

  METHOD update_line_count.

    DATA lo_displayst TYPE REF TO cl_salv_display_settings.
    DATA lv_title TYPE lvc_title.

    lo_displayst = o_alv->get_display_settings( ).
    lv_title = |{ lines( r_data->requests ) } Request(s)|.
    lo_displayst->set_list_header( lv_title ).

  ENDMETHOD.



  METHOD on_line_click.

    DATA(lv_row) = row. " lt_rows[ 1 ].

    DATA(lr_requests) = r_data->get_requests( ).

    CLEAR r_data->request_selected.
    READ TABLE lr_requests INTO r_data->request_selected INDEX lv_row.


    CASE column.
      WHEN 'TRKORR'.
        DATA(selection) = VALUE trwbo_selection(  trkorrpattern = r_data->request_selected-trkorr ).
        CALL FUNCTION 'TR_SEARCH_AND_DISPLAY_REQUESTS'
          EXPORTING
*           iv_username            = SY-UNAME
            is_selection           = selection
*           iv_title               =
*           iv_f4                  = ' '
            iv_via_selscreen       = abap_false
*           iv_complete_requests   = 'X'
*           iv_cua_status          =
*           it_special_requests    =
*          IMPORTING
*           es_selected_request    =
*           es_selected_task       =
          EXCEPTIONS
            action_aborted_by_user = 1
            OTHERS                 = 2.
        IF sy-subrc <> 0.
          MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                     WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
        ENDIF.

      WHEN 'ICON_TR_LOG'.
        IF r_data->request_selected-icon IS INITIAL.
          RETURN.
        ENDIF.

        SUBMIT rddprott AND RETURN
               WITH pv_korr  = r_data->request_selected-trkorr.

        RETURN.

      WHEN  'ICON'.

        DATA lv_answer TYPE char1.
        CASE r_data->request_selected-icon(3).
          WHEN icon_copy_object(3).

*            DATA(lv_confirm) = lcl_popup=>confirm_yes_no(
*              question = |Deseja criar uma CÓPIA da request { r_data->request_selected-trkorr } com destino ao QAS?| ).
*
*            IF lv_confirm = abap_true.

              r_data->cria_copia( ).

              r_data->select_data( ).
              me->update_line_count(  ).
              o_alv->refresh(  ).

*            ENDIF.

          WHEN icon_transport(3).

            DATA(lv_conf_lib) = lcl_popup=>confirm_yes_no(
              question = |Deseja liberar a request { r_data->request_selected-trkorr } para o QAS?| ).

            IF lv_conf_lib = abap_true.

              r_data->libera_ordem( ).

              r_data->select_data( ).
              me->update_line_count(  ).
              o_alv->refresh(  ).

            ENDIF.

          WHEN icon_led_inactive(3).

            DATA(lv_conf_imp) = lcl_popup=>confirm_yes_no(
              question = |Deseja IMPORTAR a request { r_data->request_selected-trkorr } para o { lcl_transporte_copia=>c_qa_sysid }?| ).

            IF lv_conf_imp = abap_true.

              r_data->importa_destination( ).

              r_data->select_data( ).
              me->update_line_count(  ).
              o_alv->refresh(  ).

            ENDIF.

        ENDCASE.

    ENDCASE.

  ENDMETHOD.



  METHOD set_transporte_copia.
    me->r_data = object.
  ENDMETHOD.

ENDCLASS.


**********************************************************************
TABLES: e070.

SELECT-OPTIONS s_req FOR e070-trkorr.
PARAMETERS: p_owner TYPE sy-uname DEFAULT sy-uname OBLIGATORY.
PARAMETERS: p_upto TYPE i DEFAULT 1000.

**********************************************************************

INITIALIZATION.
  DATA(data) = NEW lcl_transporte_copia( ).

AT SELECTION-SCREEN.
  data->set_selection_input( input = VALUE #(
    requests_range = s_req[]
    owner          = p_owner
    upto           = p_upto
  ) ).

START-OF-SELECTION.

  data->select_data( ).

  DATA(alv) = NEW lcl_alvgrid( ).
  alv->set_transporte_copia( data ).
  alv->chama_alv( ).












  "EOF