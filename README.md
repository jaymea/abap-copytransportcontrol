
# Programa para criação de transporte de cópias de requests


Versão 0.81 - 04.06.2020

Autor: Jayme Alonso

## Funcionalidades

1. Exibe informações das requests de workbench e cópia.
2. Permite a navegação para o LOG de transporte das requests e lista de objetos.
3. Cria uma cópia da request de workbench contendo com todos os objetos da request original e das tasks abaixo dela.
4. Libera a request de transporte de cópias para o QAS e realiza a importação sem precisar acessar a transação STMS.

## Obs: 

* Este programa não tem intuito de substituir o Transport Organizer. (SE01/SE09/SE10)
* Este programa foi desenhado para ser transportável para outros ambientes com um esforço mínimo. Por este motivo isso não foram criados STATUS GUIs / TITLEs e outros objetos que são mais difíceis de documentar.

##  BUG - O erro abaixo é conhecido:
*    Após a liberação da request existe uma demora de 5-10 segundos antes que se possa realizar a importação da request no QAS (STMS). Caso não exista a espera esse tempo é possivel que apareça um pop up com um erro no transporte. Enquanto esse bug não é tratado, como alternativa, é possível repetir o transporte até que ele dê certo.

## ANTES DE EXECUTAR:
### 1. Modificar as CONSTANTS abaixo de acordo com o ambiente destino:
 * lcl_transporte_copia=>c_qa_sysid
 * lcl_transporte_copia=>c_qa_client
### 2. Adicionar Textos:
 * P_OWNER Usuário "dono" da request
 * P_UPTO  Max reg.
 * S_REQ   Ordem/tarefa

## Log de alteracões

```

 03.02.2020 - Versão 0.5
              - Inicial
 26.02.2020 - Versão 0.6
              - Adicionado Request Superior Baseado em relação da
              tabela E071
 27.02.2020 - Versão 0.7
              - Adicionando exibição de detalhes da request quando
              clica na coluna
              - Atualizando contagem de requests quando cria uma nova
 08.05.2020 - Versão 0.8
              - Removendo a pergunta se deseja copiar a request, pois
              não é necessário devido ao fato que a criação pode ser
              cancelada no pop up exibido.
 04.06.2020 - Versão 0.81
              - Renomeando zteste_jayme_tr_copias >> ztr_copias
```